#!/usr/bin/env python

from setuptools import setup, find_packages


setup(
    name='tango-epicea',
    version='2.0.1',
    description="Epicea ISG bpm Tango server",
    package_dir={"":"./" },
    packages=["epicea"],
    entry_points={"console_scripts": [
        "Epicea = epicea.ElettraElectrometerDS:main",
        "ElettraElectrometer = epicea.ElettraElectrometerDS:main",
        "ElettraElectrometerDS = epicea.ElettraElectrometerDS:main",
    ]},
)
