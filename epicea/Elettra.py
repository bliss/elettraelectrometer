# import sys
import time

# import types
# import logging
import gevent
import gevent.event
import socket
import struct
import collections
import itertools
import math
from functools import reduce

######################################################################
###########################                ###########################
###########################  ELETTRA COMM  ###########################
###########################                ###########################
######################################################################


class ElettraDevice:

    def __init__(self, hostport):
        self.host, self.port = hostport.split(":")
        self.port = int(self.port)
        self.comm = None

    def connect(self, timeout=1):
        self.comm = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.comm.connect((self.host, self.port))
        self.comm.settimeout(timeout)

    def close(self):
        self.comm.close()

    def reboot(self):

        if self.comm is not None:
            print(("ElettraDevice::reboot, close", self.comm))
            self.comm.close()

        self.boot = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.boot.connect((self.host, 30704))
        print(("             ::reboot, connect", self.boot))

        self.boot.sendall("\x1b".encode())
        time.sleep(0.02)
        self.boot.sendall("\x07".encode())
        time.sleep(0.02)
        self.boot.sendall("\x00".encode())
        time.sleep(0.02)
        self.boot.sendall("\x00".encode())
        time.sleep(0.02)
        self.boot.sendall("\x00".encode())
        time.sleep(0.02)
        self.boot.sendall("\x03".encode())
        time.sleep(0.02)
        self.boot.sendall("\x00".encode())
        time.sleep(0.02)
        self.boot.sendall("\x00".encode())
        time.sleep(0.02)
        self.boot.sendall("\x00".encode())
        time.sleep(0.02)

        time.sleep(0.5)

        self.boot.sendall("\x1B".encode())
        time.sleep(0.02)
        self.boot.sendall("\x07".encode())
        time.sleep(0.02)
        self.boot.sendall("\x00".encode())
        time.sleep(0.02)
        self.boot.sendall("\x00".encode())
        time.sleep(0.02)
        self.boot.sendall("\x00".encode())
        time.sleep(0.02)
        self.boot.sendall("\x00".encode())
        time.sleep(0.02)
        self.boot.sendall("\x00".encode())
        time.sleep(0.02)
        self.boot.sendall("\x00".encode())
        time.sleep(0.02)
        self.boot.sendall("\x00".encode())
        time.sleep(0.02)

        print(("             ::reboot, close", self.boot))
        self.boot.close()
        time.sleep(2)
        print("             ::reboot, done")

    def write(self, byts):
        ret = self.comm.sendall(byts)
        return ret

    def read(self, nbytes=8192):
        return self.comm.recv(nbytes)

    def putget(self, command, no_cr=False, has_answer=True, has_multi_ans=False):

        cmd = "%s%s" % (command, "" if no_cr else "\r")

        try:
            return self._putget(cmd, has_answer, has_multi_ans)
        except AssertionError:
            print("ElettraDevice::Device communication assertion error. try again.")
            return self._putget(cmd, has_answer, has_multi_ans)
        except socket.timeout:
            print("ElettraDevice::Socket communication timed out.")
        except socket.error as e:
            print(("ElettraDevice::Socket communication error:", e))
            print("             ::Closing connection and reconnecting to device.")
            self.close()
            self.connect()
            return self._putget(cmd, has_answer, has_multi_ans)

    def _putget(self, cmd, has_answer, has_multi_ans):

        with gevent.Timeout(0.5):
            self.write(cmd.encode())
            if has_answer:
                read_buffer = ""

                while not read_buffer.endswith("\r\n"):
                    data = self.read()
                    if not data:
                        return
                    read_buffer += data.decode()

                answer = [_f for _f in read_buffer.split("\r\n") if _f]

                for ii in range(len(answer)):
                    print(("             ::putget: ", ii + 1, answer[ii]))

                if has_multi_ans is False:
                    # we cannot receive more than one answer!
                    assert len(answer) == 1

                    if "?" in cmd:
                        assert answer[0].startswith(cmd.split()[0])
                        return " ".join(answer[0].split(" ")[1:])
                    else:
                        return answer[0]  # will be ACK or NAK
                else:
                    return read_buffer

            return None


######################################################################
###################                                ###################
###################  ELETTRA FOURCHANNEL COMMANDS  ###################
###################                                ###################
######################################################################


class FourChannel:

    def __init__(
        self,
        hostport,
        buffer_size=32820,
        channels=4,
        convspeed="FAST",
        geometry="regular",
        binary=0,
        model=None,
    ):
        # 32820 corresponds roughly to an acquisition of 10 seconds with AH501

        self.device_hostport = hostport
        self.buffer_size = buffer_size
        self.diode_geometry = geometry
        self.binary_mode = binary

        self.elettra_device = ElettraDevice(hostport)
        self.model = model

        self.isAH501 = False
        self.isAH401 = False

        if "501" in self.model:
            self.isAH501 = True

        if "401" in self.model:
            self.isAH401 = True

        self.acquisition_task = None
        self._stop_acquisition_event = gevent.event.Event()
        self.__init_buffer()
        self._n_channels = channels
        self._channel_bits = 24
        self.range_map = []
        self.conv_speed = convspeed
        self.range = -1
        self.itm = 0
        self.fsc = 1.0
        self.bin_offset = [0, 0, 0, 0]
        self.scale_factor = -1
        self.min_sampling_time = 1.0

    def __init_buffer(self):
        self._data_buffer = collections.deque(maxlen=self.buffer_size)
        self._time_stamp = collections.deque(maxlen=self.buffer_size)
        self._time_reado = collections.deque(maxlen=self.buffer_size)
        self._read_buffer = ""
        self._stop_acquisition_event.clear()

    ######################################################################
    ########################                      ########################
    ######################## COMMON DEVICES FUNCS ########################
    ########################                      ########################
    ######################################################################

    def putget(self, *args, **kwargs):
        if self.acq_is_running():
            raise RuntimeError("command not allowed while acquisition is running")

        ret = self.elettra_device.putget(*args, **kwargs)
        return ret

    def reboot(self):
        self.elettra_device.reboot()

    def connect(self):

        try:
            self.elettra_device.connect()
            self._loadACQ_OFF()
            self.getModel()

        except:
            print(("           ::Cannot establish connection to", self.device_hostport))
            print("           ::Please rReboot the Electrometer controller, and retry.")
            return None

        print((
            "           ::Connected to Elettra %s (%s) (%s)"
            % (self.model[0], self.model[1], self.model)
        ))

        print("           ::Disabling External Trigger.")
        self.setTriggerOff()

        if "501" in self.model:
            print("           ::Disabling DEC.")
            self.putget("DEC OFF")
            print(("           ::Setting %d channels." % (self._n_channels)))
            self.putget("CHN %s" % (self._n_channels))
            print("           ::Setting 24 bits resolution.")
            self.putget("RES 24")  # self._channel_bits
            self.range_map = [2.5e-3, 2.5e-6, 2.5e-9]
            self.isAH501 = True
            self.isAH401 = False
            self.meas_offset = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]

        if "401" in self.model:
            print("           ::Disabling NAQ function.")
            self.putget("NAQ 0")
            print("           ::Disabling SUM mode.")
            self.putget("SUM OFF")
            self.range_map = [
                2e-9,
                50e-12,
                100e-12,
                150e-12,
                200e-12,
                250e-12,
                300e-12,
                350e-12,
            ]
            self.isAH501 = False
            self.isAH401 = True
            self.meas_offset = [0, 0, 0, 0]

        if self.binary_mode:
            print("           ::Setting Binary Mode On.")
            self.setBinaryOn()
        else:
            print("           ::Setting Ascii Mode On.")
            self.setBinaryOff()

        self.getFullScaleCurrent()

        return self.elettra_device.comm

    def disconnect(self):
        self.elettra_device.close()

    def getModel(self):

        if not self.acq_is_running():
            self.model = self.putget("VER ?")

        print(("           ::", self.model))
        return self.model

    def setBinaryOn(self):
        if self.binary_mode == 0:
            sign = list(map(cmp, self.bin_offset, [0, 0, 0, 0]))
            self.bin_offset = list(map(lambda x, y: y * (abs(x) << 8), self.bin_offset, sign))

        self.binary_mode = 1
        self.calcScaleFactor()
        return self.putget("BIN ON")

    def setBinaryOff(self):
        if self.binary_mode == 1:
            sign = list(map(cmp, self.bin_offset, [0, 0, 0, 0]))
            self.bin_offset = list(map(lambda x, y: y * (abs(x) >> 8), self.bin_offset, sign))

        self.binary_mode = 0
        self.calcScaleFactor()
        return self.putget("BIN OFF")

    def getBinary(self):
        return self.putget("BIN ?")

    def setTriggerOn(self):
        return self.putget("TRG ON")

    def setTriggerOff(self):
        return self.putget("TRG OFF")

    def getTrigger(self):
        return self.putget("TRG ?")

    def setChannel(self, num):
        pass

    def getChannel(self):
        return self._n_channels

    def setRange(self, idx):

        if idx < 0 or idx >= len(self.range_map):
            raise RuntimeError("bad range values")
        else:
            # TODO this is too fast for AH401
            acq_stopped = self.stopAcq()
            self.range = idx
            self._loadRNG()
            self.getRange()
            if self.range != idx:
                raise RuntimeError("badd range values")
            if acq_stopped:
                self.startContinuousAcq()

        return self.range

    def _loadRNG(self):
        self.putget("RNG %s" % (self.range))

    def getRange(self):
        if not self.acq_is_running():
            self.range = int(self.putget("RNG ?")[0])
            self.calcScaleFactor()

        print(("           ::RNG is %s" % self.range))
        return self.range

    def correctInstrumentOffset(self, newoffset):
        self.bin_offset = newoffset

    def setMeasureOffset(self, itime=1):

        if itime <= 0:
            itime = 1

        self.stopAcq()
        self.startAcq(itime)
        print("FourChannel::setMeasureOffset: measuring ....")
        time.sleep(itime)

        full_length = len(self._time_stamp)
        print((
            "FourChannel::setMeasureOffset::slice _data_buffer from %d to the end %d"
            % (0, full_length - 1)
        ))

        _data = list(itertools.islice(self._data_buffer, 0, full_length))

        if "501" in self.model:
            for channum in range(self._n_channels):
                self.meas_offset[self.range][channum] = (
                    reduce(lambda x, y: x + y, [x[channum] for x in _data])
                    / full_length
                ) * self.scale_factor

        if "401" in self.model:
            for channum in range(self._n_channels):
                self.meas_offset[channum] = (
                    reduce(lambda x, y: x + y, [x[channum] for x in _data])
                    / full_length
                ) * self.scale_factor

        return self.meas_offset

    def _setMeasureOffset(self, value):
        self.meas_offset = value

    def resetMeasureOffset(self):
        if "501" in self.model:
            self.meas_offset[self.range] = [0, 0, 0, 0]

        if "401" in self.model:
            self.meas_offset = [0, 0, 0, 0]

        return self.meas_offset

    def getMeasureOffset(self):
        if "501" in self.model:
            return self.meas_offset[self.range]

        if "401" in self.model:
            return self.meas_offset

    ######################################################################
    ############################              ############################
    ############################  AH401 only  ############################
    ############################              ############################
    ######################################################################

    def setItime(self, value):
        print("FourChannel::setItime")
        if not self.isAH401:
            raise RuntimeError("not allowed for this model")
        elif value < 1 or value > 10000:
            raise RuntimeError("invalid integration time")
        else:
            acq_stopped = self.stopAcq()
            value = int(value)
            self.itm = value
            self._AH401_loadITM()
            self.getItime()
            if self.itm != value:
                raise RuntimeError("wrong integration time")
            if acq_stopped:
                self.startContinuousAcq()

        return self.itm

    def _AH401_loadITM(self):
        print((
            "FourChannel::_AH401_loadITM %s : %s"
            % (self.itm, self.putget("ITM %s" % (self.itm)))
        ))

    def getItime(self):
        print("FourChannel::getItime")
        if self.isAH401:
            if not self.acq_is_running():
                self.itm = int(self.putget("ITM ?"))
                self.calcScaleFactor()

            print(("           ::ITM is %s" % (self.itm)))
            return self.itm
        else:
            raise RuntimeError("not allowed for this model")

    def setHLF(self, value):
        print("FourChannel::setHLF")
        if not self.isAH401:
            raise RuntimeError("not allowed for this model")

        if value == "ON":
            self.conv_speed = "SLOW"
            print((
                "FourChannel::setHLF: Setting LOW NOISE mode (HLF %s) : %s"
                % ("ON", self.putget("HLF %s" % ("ON")))
            ))
        else:
            self.conv_speed = "FAST"
            print((
                "FourChannel::setHLF: Setting FAST mode (HLF %s) : %s"
                % ("OFF", self.putget("HLF %s" % ("OFF")))
            ))

    def getHLF(self):
        print("FourChannel::getHLF")
        if self.isAH401:
            value = self.putget("HLF ?")
            print(("           ::HLF is %s" % (value)))
            return value
        else:
            raise RuntimeError("not allowed for this model")

    ######################################################################
    ############################              ############################
    ############################  AH501 only  ############################
    ############################              ############################
    ######################################################################

    def setResolution(self, num):
        print("FourChannel::setResolution")
        if not self.isAH501:
            raise RuntimeError("not allowed for this model")
        print((
            "FourChannel::setResolution: Setting Resolution to %d bits: %s"
            % (num, self.putget("RES %d" % (num)))
        ))

    def getResolution(self):
        print("FourChannel::getResolution")
        if not self.isAH501:
            raise RuntimeError("not allowed for this model")
        return self.putget("RES ?")

    def setBias(self, value):
        print("FourChannel::setBias")
        if self.isAH501:
            print((
                "FourChannel::setBias: Setting Bias  %s : %s"
                % (value, self.putget("HVS %s" % (value)))
            ))
        else:
            raise RuntimeError("not allowed for this model")

    def getBias(self):
        print("FourChannel::getBias")
        if self.isAH501:
            return self.putget("HVS ?")
        else:
            raise RuntimeError("not allowed for this model")

    ######################################################################
    ##########################                 ###########################
    ##########################   ACQUISITION   ###########################
    ##########################                 ###########################
    ######################################################################

    def acquire(self, itime, wait=True):
        print(("FourChannel::acquire", itime, wait))
        self.startAcq(itime)

        if wait:
            return self.acquisition_task.get()
        else:
            return self.acquisition_task

    def acq_is_running(self):
        return self.acquisition_task is not None and not self.acquisition_task.ready()

    def startAcq(self, itime=0):
        # time 0 means infinite acquisition (until stopAcq is called)
        print(("FourChannel::startAcq", itime))

        if self.acq_is_running():
            raise RuntimeError("not allowed for this model")

        self.__init_buffer()
        self.acquisition_task = gevent.spawn(self._do_acquisition, itime)
        print(("           ::", self.acquisition_task))

        return self.acquisition_task

    def startContinuousAcq(self):
        print("FourChannel::startContinuousAcq")

        if not self.acq_is_running():  # if new acq starts resets buffer
            self.__init_buffer()
            self.acquisition_task = gevent.spawn(self._do_acquisition)

        else:
            print("           ::already running ContinuousAcq.")

        print(("           ::", self.acquisition_task))

    def stopAcq(self):
        print(("FourChannel::stopAcq", self.acquisition_task))

        if self.acq_is_running():
            print("           ::acq task running, setting stop event")
            self._stop_acquisition_event.set()
            print("           ::stop event set that will join acq task")
            self.acquisition_task.join()
            print("           ::acquisition stopped.")
            return 1
        else:
            print("           ::already stopped.")
            return 0

    def _do_acquisition(self, itime=0):

        print(("FourChannel::_do_acquisition itime %g" % (itime)))

        self._read_buffer = ""
        self._time_origin = time.time()
        self._loadACQ_ON()

        g = None
        if itime > 0:
            print((
                "           ::spawning _stop_acquisition_event in %g seconds." % itime
            ))
            g = gevent.spawn_later(itime, self._stop_acquisition_event.set)

        #        _tloop=self._time_origin
        _tloopstat = [0, 0]

        while not self._stop_acquisition_event.is_set():
            # print ("FourChannel::_do_acquisition time at loop start:%g" %)
            # (time.time()-self._time_origin)
            print(("--", _tloopstat))

            try:
                _tloop = time.time()

                gevent.sleep(0.005)
                data = self.elettra_device.read(2048)
                data = data.decode()
                if data == "":
                    self.__init_buffer()
                    return data

                self.parseReadingsFromDevice(data)

                #                    print ("FourChannel::_do_acquiistion time after read + parse:%g" % (time.time()-self._time_origin))
                # print ("FourChannel::_do_acquisition time for read + parse:%g" %)
                # (time.time()-_tloop)

                if len(self._data_buffer) != len(self._time_stamp):
                    raise RuntimeError("unconsistent buffer")

                _tloopstat[1] = (_tloopstat[0] * _tloopstat[1]) + (time.time() - _tloop)
                _tloopstat[0] += 1
                _tloopstat[1] /= _tloopstat[0]

                if (time.time() - _tloop) > (_tloopstat[1] * 15):
                    print((
                        "DESYNCHRONISATION DETECTED", time.asctime(), _tloopstat[0] - 1
                    ))

            except Exception as e:
                if e.args[0] == "timed out":
                    pass
                else:
                    print(("FourChannel::_do_acquisition loop error:", e.args))

        print(("           ::stopping _do_acquisition", time.time() - self._time_origin))

        # when AH401/501 device is stopped it sends last readings away...
        self.parseReadingsFromDevice(self._loadACQ_OFF())

        if g is not None:
            g.kill()

        print((
            "           ::exiting _do_acquisition loop", time.time() - self._time_origin
        ))

    def _loadACQ_ON(self):
        print("FourChannel::_loadACQ_ON()")

        if self.isAH401:
            self.elettra_device.putget("ACQ ON", has_answer=True)
        elif self.isAH501:
            self.elettra_device.putget("ACQ ON", has_answer=False)

    def kill501(self):
        if self.isAH501:
            self.elettra_device.putget("S", no_cr=True)

    def _loadACQ_OFF(self):
        print("FourChannel::_loadACQ_OFF()")

        last_read = ""
        if self.isAH501:
            last_read = self.elettra_device.putget("S", no_cr=True)
        elif self.isAH401:
            last_read = self.elettra_device.putget("ACQ OFF")
        last_read = last_read.replace("ACK", "")

        return last_read

    def parseReadingsFromDevice(self, bytes_read):

        #        print ("FourChannel::parseReadingsFromDevice::_read       : " , bytes_read)
        #        print ("                                    ::_read_buffer: " , self._read_buffer)
        #        print ("                                    ::_read       : " , len(bytes_read))
        # print ("                                    ::_read_buffer: " ,)
        # len(self._read_buffer)

        if self.binary_mode:
            return self.parseBinaryReadingsFromDevice(bytes_read)
        else:
            return self.parseAsciiReadingsFromDevice(bytes_read)

    def parseBinaryReadingsFromDevice(self, bytes_read):

        self._read_buffer += bytes_read

        _channel_bytes = self._channel_bits / 8
        _sample_bytes = self._n_channels * _channel_bytes

        if len(self._read_buffer) < _sample_bytes:
            return []

        _no_samples = len(self._read_buffer) / _sample_bytes
        _time_value = time.time() - self._time_origin

        if len(self._time_stamp) > 0:
            _time_stamp = self._time_stamp[len(self._time_stamp) - 1]
        else:
            _time_stamp = 0

        # could be also estimated
        _time_sample = (_time_value - _time_stamp) / _no_samples
        # AH501 24bit 3000 samples/sec => 3.333e-4
        # AH401 ITIM 10 => 1e-3
        i = 0
        readings = []
        timestamp = []
        rdouttime = []

        # print ("FourChannel::parseReadingsFromDevice::sampling period)
        # is",_time_sample
        print((
            "                                    ::samplingrate is",
            int(_no_samples / (_time_value - _time_stamp)),
        ))
        #        print ("                                    ::nosamples",_no_samples)

        while i + _sample_bytes <= len(self._read_buffer):

            ttt = time.time()
            raw_channels_value = struct.unpack(
                ("%ds" % _channel_bytes) * self._n_channels,
                self._read_buffer[i : i + _sample_bytes],
            )

            # print (")
            # :::raw_channels_value",raw_channels_value

            if self.isAH501:  # MSB first, big endian, std size 32bits
                channels_value = list(
                    struct.unpack(
                        ">" + "l" * self._n_channels,
                        ("%s\0" * self._n_channels) % raw_channels_value,
                    )
                )

            elif self.isAH401:  # LSB first, little endian, std size 32bits
                channels_value = list(
                    struct.unpack(
                        "<" + "l" * self._n_channels,
                        ("\0%s" * self._n_channels) % raw_channels_value,
                    )
                )

            #            print ("                                    :::channels_value %s" % (channels_value))
            # print ("                                    :::bin_offset %s" %)
            # (self.bin_offset)

            channels_value = list(map(lambda x, y: x - y, channels_value, self.bin_offset))

            _time_stamp += _time_sample

            readings.append(channels_value)
            rdouttime.append(_time_value)
            timestamp.append(_time_stamp)
            i += _sample_bytes

        # print ("                                    :::timestamp %g timevalue %g)
        # channels_value %s timeloop %g" % (_time_stamp, _time_value,
        # channels_value,time.time()-ttt)
        self._read_buffer = self._read_buffer[i:]

        self._time_stamp.extend(timestamp)
        self._time_reado.extend(rdouttime)
        self._data_buffer.extend(readings)

        return len(readings)

    def parseAsciiReadingsFromDevice(self, str_read):

        self._read_buffer += str_read

        _no_samples = self._read_buffer.count("\r\n")

        if _no_samples < 1:
            return []

        samples = self._read_buffer.split("\r\n")
        _time_value = time.time() - self._time_origin
        if len(self._time_stamp) > 0:
            _time_stamp = self._time_stamp[len(self._time_stamp) - 1]
        else:
            _time_stamp = 0
        _time_sample = (_time_value - _time_stamp) / _no_samples

        i = 0
        readings = []
        timestamp = []
        rdouttime = []

        print(("FourChannel::parseReadingsFromDevice::sampling period is", _time_sample))
        print((
            "                                    ::samplingrate",
            int(_no_samples / (_time_value - _time_stamp)),
        ))
        print(("                                    ::nosamples", _no_samples))
        #        print ("                                    ::samples",samples)

        while i < _no_samples:

            ttt = time.time()

            #            print ("                                    :::_read_buffer",self._read_buffer)
            # print (")
            # :::samples",samples[i],"i",i,"/",_no_samples

            channels_value = samples[i].split()
            self._read_buffer = self._read_buffer.lstrip(samples[i])
            self._read_buffer = self._read_buffer.lstrip("\r\n")

            _time_stamp += _time_sample

            if len(channels_value) != self._n_channels:
                print(("CORRUPTED, DATA DISCARDED", time.asctime(), channels_value, i))
                i += 1
                continue

            # print (")
            # :::channels_value",channels_value

            readings.append(list(map(self.__ascii2int, channels_value, self.bin_offset)))
            rdouttime.append(_time_value)
            timestamp.append(_time_stamp)

            i += 1

        # print ("                                    :::timestamp %g timevalue %g)
        # channels_value %s timeloop %g" % (_time_stamp, _time_value,
        # channels_value,time.time()-ttt)

        self._time_stamp.extend(timestamp)
        self._time_reado.extend(rdouttime)
        self._data_buffer.extend(readings)

        return len(readings)

    def __ascii2int(self, str, offset):

        if "401" in self.model:
            val = int(str)
            val -= offset

        if "501" in self.model:
            val = int(str, 16)
            val -= offset

            if val >= 0x7fffff:
                val = val - 0xffffff

        return val

    def getBuffer(self, n=0):
        print(("FourChannel::getBuffer n=%d." % (n)))
        full_length = len(self._data_buffer)
        if full_length == 0:
            raise RuntimeError("buffer is empty")
        ret = list(
            itertools.islice(
                self._data_buffer, full_length - n if n else 0, full_length
            )
        )
        print(("           ::Buffer length is %d." % (full_length - n)))
        return ret

    def getTimeStamp(self, n=0):
        print(("FourChannel::getTimeStamp n=%d." % (n)))
        full_length = len(self._time_stamp)
        if full_length == 0:
            raise RuntimeError("buffer is empty")
        ret = list(
            itertools.islice(self._time_stamp, full_length - n if n else 0, full_length)
        )
        print(("           ::TimeStamp length is %d." % (full_length - n)))
        return ret

    def getTimeStampReadout(self, n=0):
        print(("FourChannel::getTimeStampReadout n=%d." % (n)))
        full_length = len(self._time_reado)
        ret = list(
            itertools.islice(self._time_reado, full_length - n if n else 0, full_length)
        )
        print(("           ::TimeStampReadout length is %d." % (full_length - n)))
        return ret

    def getMeasure(self, itime=0):
        # 0 means average whole buffer, itime larger than buffer does so.
        # calculates : sum1 sum2 sum3 sum4 tstamp noitems
        # future x y  cur1 cur2 cur3 cur4 alarms ...

        print(("FourChannel::getMeasure itime=%g." % (itime)))

        full_length = len(self._time_stamp)
        if full_length == 0:
            raise RuntimeError("full length is 0")

        ret = []
        full_time = self._time_stamp[full_length - 1]

        print(("           ::full time is %g." % (full_time)))

        if full_time < itime:
            itime = 0

        if itime == 0:
            n = 0
        else:
            tstamp = self._time_stamp[full_length - 1] - itime

            for n, item in enumerate(list(self._time_stamp)):
                if item > tstamp:
                    break

        print((
            "           ::slice _data_buffer from %d to the end %d"
            % (n, full_length - 1)
        ))
        _data = list(itertools.islice(self._data_buffer, n, full_length))

        if "501" in self.model:
            idx = self.range
            offset = self.meas_offset[idx]

        if "401" in self.model:
            offset = self.meas_offset

        nn = full_length - n

        print(("           ::instrument offset", self.bin_offset))
        print(("           ::measuremnt offset", self.meas_offset))
        #        print ("           ::global offset",offset)
        print(("           ::samples number", nn))
        print(("           ::self.scale_factor", self.scale_factor))
        #        print ("           ::_data",_data)

        for channum in range(self._n_channels):
            print((
                "reduced data::",
                reduce(lambda x, y: x + y, [x[channum] for x in _data]) / nn,
            ))
            ret.append(
                (reduce(lambda x, y: x + y, [x[channum] for x in _data]) / nn)
                * self.scale_factor
                - offset[channum]
            )

        # n-1 allows to count in itime, int. time (401) of first value in
        # sliced _data.
        itime = self._time_stamp[full_length - 1]
        if n > 0:
            itime -= self._time_stamp[n - 1]

        if self._n_channels is 4:
            y = self.getY(ret[0:4])
            z = self.getZ(ret[0:4])
        else:
            y = -1
            z = -1

        ret.append(itime)
        ret.append(nn)
        ret.append(y)
        ret.append(z)
        ret.append(sum(ret[0:4]))

        #        print ("           ::Y %g" % (y))
        #        print ("           ::Z %g" % (z))
        print(("           ::ret", ret))

        return ret

    def getY(self, measure):
        print(("FourChannel::getY", self.diode_geometry, measure))
        if not isinstance(measure,list):
           return -1

        if (self.diode_geometry.lower() == "diagonal") or (
            self.diode_geometry.lower() == "diamond"
        ):
            _sum2 = measure[1] + measure[3]
            if _sum2 != 0:
                return (measure[3] - measure[1]) / float(_sum2)

        elif (self.diode_geometry.lower() == "toolazyforcabling") or (
            self.diode_geometry.lower() == "diamond90"
        ):
            _sum2 = measure[0] + measure[2]
            if _sum2 != 0:
                return (measure[2] - measure[0]) / float(_sum2)

        else:
            try:
                _sum = math.fsum(measure[0:4])
            except BaseException as e:
                 raise BaseException("cannot sum values %s" % str(measure[0:4]))

            if _sum != 0:
                return (measure[0] + measure[1] - measure[2] - measure[3]) / _sum

        return 0

    def getZ(self, measure):
        print(("FourChannel::getZ", self.diode_geometry, measure))

        if (self.diode_geometry.lower() == "diagonal") or (
            self.diode_geometry.lower() == "diamond"
        ):
            _sum2 = measure[0] + measure[2]
            if _sum2 != 0:
                return (measure[0] - measure[2]) / float(_sum2)

        elif (self.diode_geometry.lower() == "toolazyforcabling") or (
            self.diode_geometry.lower() == "diamond90"
        ):
            _sum2 = measure[3] + measure[1]
            if _sum2 != 0:
                return (measure[3] - measure[1]) / float(_sum2)

        else:
            _sum = math.fsum(measure[0:4])
            if _sum != 0:
                return (measure[0] - measure[1] - measure[2] + measure[3]) / _sum

        return 0

    ######################################################################
    ########################                    ##########################
    ######################## FULL SCALE CURRENT ##########################
    ########################                    ##########################
    ######################################################################

    def getFullScaleCurrent(self):
        print("FourChannel::getFullScaleCurrent")

        if not self.acq_is_running():
            self.getRange()
            if self.isAH401:
                self.getItime()

        print(("           ::fsc is %g" % self.fsc))
        return self.fsc

    def calcScaleFactor(self):
        print("FourChannel::calcScaleFactor")
        if self.isAH501:
            self.fsc = self.range_map[self.range]
            if 1:  # 24 bit mode
                if self.binary_mode:
                    self.scale_factor = -self.fsc / 0x80000000
                else:
                    self.scale_factor = -self.fsc / 0x800000
            else:  # 16 bit mode (not further implemented)
                self.scale_factor = -self.fsc / 0x8000

        elif self.isAH401:
            if self.itm:
                self.fsc = self.range_map[self.range] / self.itm * 10000
                if self.binary_mode:
                    self.scale_factor = self.fsc / (0x0FFFFF00)
                else:
                    self.scale_factor = self.fsc / (0xFFFFF)

    def getScaleFactor(self):
        print("FourChannel::getScalefactor")
        return self.scale_factor

    def setFullScaleCurrent(self, value):
        print((
            "FourChannel::setFullScaleCurrent: Setting fsc for measuring %g Amps."
            % (value)
        ))
        self._setGainSettings(1.15 * value, self.min_sampling_time)
        return self.getFullScaleCurrent()

    def getMinSamplingTime(self):
        print(("FourChannel::getMinSamplingTime", self.min_sampling_time))
        if self.isAH401:
            return self.min_sampling_time
        else:
            return -1

    def setMinSamplingTime(self, value):
        print(("FourChannel::setMinSamplingTime", value))
        if value > 2.0:
            value = 2.0
        self.min_sampling_time = value
        self._setGainSettings(self.fsc, value)
        return self.min_sampling_time

    def _setGainSettings(self, tgt_fscale, min_sampltime):
        print(("FourChannel::_setGainSettings: %g %d." % (tgt_fscale, min_sampltime)))
        acq_stopped = self.stopAcq()
        #        self.min_sampling_time = min_sampltime

        if self.isAH501:
            for _rng in [2, 1, 0]:
                if tgt_fscale < self.range_map[_rng]:
                    break
            print(("           ::_setGainSettings: %g." % (self.range_map[_rng])))

        elif self.isAH401:
            # there is a factor 2 due to the double integrator!!
            min_sampltime /= 2

            if min_sampltime > 0.02:  # if > 20ms(50Hz) round to multiple
                tgt_sampltime = int(min_sampltime / 0.02) * .02
            else:
                tgt_sampltime = min_sampltime

            for _rng in range(1, 8):
                if tgt_fscale < (self.range_map[_rng] / tgt_sampltime):
                    _itm = 10000 * tgt_sampltime
                    break
            else:
                _rng = 0
                sampltime = self.range_map[0] / tgt_fscale
                #               print (">>> ", sampltime, tgt_fscale)
                if sampltime > tgt_sampltime:
                    sampltime = tgt_sampltime
                if sampltime > 0.02:
                    sampltime = int(sampltime / 0.02) * .02
                _itm = 10000 * sampltime
                if _itm < 10:
                    _itm = 10
        #               print (">>> ", sampltime, tgt_fscale, _itm)

        self.setRange(_rng)
        if self.isAH401:
            self.setItime(_itm)

        self.calcScaleFactor()

        if acq_stopped:
            self.startContinuousAcq()


################################  END  ###############################
