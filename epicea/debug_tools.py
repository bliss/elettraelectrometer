import functools
import time
import inspect

DEBUG=False
SHOW_FILE=False
SHOW_ELAPSED=False

def debug_method(fn):
    def wrapper(*args, **kwargs):
        elapsed = _file = ''
        if DEBUG:
            if SHOW_FILE:
                _file = inspect.getfile(fn) + ":"

            _name = fn.__name__
            _cls = args[0].__class__.__name__
    
            print(f'  {_file}{_cls}.{_name}() -- Enter -- args={args[1:]} kwargs={kwargs}')
            if SHOW_ELAPSED:
               t0 = time.time()

        value=fn(*args,**kwargs) 

        if DEBUG:
            if SHOW_ELAPSED:
                t1 = (time.time()-t0)
                elapsed = f' elapsed: {elapsed}secs'

            print(f'  {_file}{_cls}:{_name}() -- Exit {elapsed}' )

        return value

    return wrapper
