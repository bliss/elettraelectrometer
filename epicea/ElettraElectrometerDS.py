#!/usr/bin/env python

import gevent
import numpy as np
import math

from epicea import Elettra
from epicea import BridgeGevent

import tango

from tango import DevState, DevVarStringArray

from tango.server import command, attribute
from tango.server import device_property

#
# run a bridge / proxy - with gevent on. start it
# at the beginning to be sure that it runs in the
# main server thread
#

bridge = BridgeGevent.BridgeGevent()
bridge.run()

class ElettraElectrometer(tango.server.Device):

    __metaclass__ = tango.server.DeviceMeta

    # device properties
    DeviceIP        = device_property(dtype=str, 
                            doc='Device IP address')
    BufferSize      = device_property(dtype=int, default_value=32320,
                            doc='Window size of internal buffer')
    ChannelNumber   = device_property(dtype=int, default_value=4,
                            doc='Number of channels')
    HLF             = device_property(dtype=str, default_value='OFF', 
                            doc='AH401 HLF ON|OFF')
    ConversionMode  = device_property(dtype=str, default_value='FAST', 
                            doc='AH401 FAST|SLOW')
    MinSamplingTime = device_property(dtype=float, default_value=1.0, 
                            doc='AH401 minimum sampling time')
    DiodeGeometry   = device_property(dtype=str, default_value='regular', 
                            doc="""
			"Arrangement of diodes : 'diagonal' , 'toolazyforcabling' or 'regular' by default
                             """)
    Binary          = device_property(dtype=int, default_value=0, 
                             doc='0:ASCII 1:BINARY')
    Model           = device_property(dtype=str, 
                             doc='AH401 or AH501')
    MeasureSingleTime  = device_property(dtype=float, default_value=1,
                          doc='Integration time for MeasureSingle')  

    # internal use
    instrument_offset  = device_property(dtype=(str,))
    measurement_offset = device_property(dtype=(str,))

    def __init__(self, *args, **kwargs):
        tango.server.Device.__init__(self, *args, **kwargs)

    def init_device(self):

        super(ElettraElectrometer,self).init_device()

        print(f'Elettra - INIT DEVICE')

        self.elettra_device = None

        # starts communication with Elettra.FourChannel class in another thread,
        # running the gevent loop

        self.db = tango.Database()
        self.device = self.get_name()

        self.ctime = 1

        if (self.HLF != "ON"):
            self.HLF="OFF"

        if (self.ConversionMode != "SLOW"):
            self.ConversionMode = "FAST"

        print(f"INIT device . IP is: {self.DeviceIP}")

        # create the device through the bridge. then use the bridge
        # for all access to the device
        bridge.create_device(Elettra.FourChannel,self.DeviceIP,
                                           self.BufferSize,
                                           self.ChannelNumber,
                                           self.ConversionMode, 
                                           self.DiodeGeometry,
                                           self.Binary, 
                                           self.Model)
        self.elettra_device = bridge

        if self.elettra_device.connect() is not None:
            try:
                self.device_model = self.elettra_device.getModel()
                if "401" in self.device_model:
                    self.elettra_device.setHLF(self.HLF)
                self.set_state(DevState.ON)
            except Exception as exc:
                self.set_state(DevState.FAULT)
                self.set_status((str(exc)))
                return
        else:
            self.set_state(DevState.FAULT)
            self.set_status('Cannot connect to %s' % self.DeviceIP)
            return

        self.db.put_device_property(self.device, {'Model': self.device_model})

        #InstrumentOffset must be init here after connection.
        #it must be in BIN unit or ASCII unit coherent with Binary property

        if self.instrument_offset:
            fvals = self.instrument_offset[0][1:-1].strip().split() # remove brackets and split
            conv_val = [ float(fval) for fval in fvals]
            self.elettra_device.correctInstrumentOffset(conv_val)
            print(f" - DB device property instrument_offset {fvals} converted into {conv_val}")

        if self.measurement_offset:
            mvals = self.measurement_offset[0][1:-1].strip()
            print(f'mvals: {mvals}')
            conv_val = eval(mvals)
            self.elettra_device._setMeasureOffset (conv_val)
            print(f" - DB device property measurement_offset {mvals} converted into {conv_val}")

    def delete_device(self):
        if self.elettra_device is not None:
            self.elettra_device.stopAcq()
            self.elettra_device.disconnect()
            self.elettra_device = None
                        
    # COMMANDS

    @command(dtype_in=str, dtype_out=str)
    def PutGet (self,command):
        return self.elettra_device.putget(command)

    @command
    def Reset(self):
        self.elettra_device.kill501()
        self.set_state(DevState.ON)

    @command
    def StopAcq(self):
        self.elettra_device.stopAcq()
        self.set_state(DevState.ON)

    @command(dtype_in=float,  doc_in='client integration time')
    def StartContinuousAcq(self, itime=0):
        self.ctime = itime
        self.elettra_device.startContinuousAcq()
        self.set_state(DevState.RUNNING)
 
    @command
    def MeasureSingle(self):
        # this is for polling purposes only (command without argument)
        # using StartContinuousAcq the electrometer gives crazy values after a while
        # not sure 'Sync' helps
        itime = self.MeasureSingleTime
        self.ctime = itime
        self.set_state(DevState.RUNNING) # does not affect state?
        self.elettra_device.acquire(itime, wait=True)
        self.elettra_device.stopAcq()
        self.set_state(DevState.ON)

    @command(dtype_in=np.int16, dtype_out=np.int16, doc_in='range')
    def setRange (self, _range):
        return self.elettra_device.setRange (_range)

    @command(dtype_out=np.int16)
    def getRange (self):
        return self.elettra_device.getRange ()

    @command(dtype_in=np.int16, dtype_out=np.int16, doc_in='itime')
    def setItime (self, itime):
        return self.elettra_device.setItime (itime)

    @command(dtype_out=int)
    def getItime (self):
        return self.elettra_device.getItime ()

    @command(dtype_in=(float,))
    def correctInstrumentOffset (self, value):
        self.elettra_device.correctInstrumentOffset (value)
        self.db.put_device_property (self.device, {'instrument_offset':[value]})

    @command(dtype_in=float, doc_in='measurement time')
    def setMeasureOffset (self, itime):
        bli=self.elettra_device.setMeasureOffset (itime)
        self.db.put_device_property (self.device, {'measurement_offset':[bli]})

    @command(dtype_out=(float,))
    def getMeasureOffset (self):
        return self.elettra_device.getMeasureOffset()

    @command
    def resetMeasureOffset (self):
        bli=self.elettra_device.resetMeasureOffset ()
        self.db.put_device_property (self.device, {'measurement_offset':[bli]})

    @command(dtype_in=float, dtype_out=float, doc_in='current')
    def setFullScaleCurrent (self, fsvalue):
        return self.elettra_device.setFullScaleCurrent(fsvalue)

    @command(dtype_out=float)
    def getFullScaleCurrent (self):
        return self.elettra_device.getFullScaleCurrent()

    @command(dtype_in=float, dtype_out=float, doc_in='mintime')
    def setMinSamplingTime (self, value):
        return self.elettra_device.setMinSamplingTime(value)

    @command(dtype_out=float)
    def getMinSamplingTime (self):
        return self.elettra_device.getMinSamplingTime()

    @command(dtype_out=float)
    def getScaleFactor (self):
        return self.elettra_device.getScaleFactor()

    @command(dtype_in=str, doc_in='bias voltage')
    def setBias (self, bias):
        if self.get_state() == DevState.ON:
            self.elettra_device.setBias (bias)

    @command(dtype_out=str)
    def getBias (self):
        if self.get_state() == DevState.ON:
            return self.elettra_device.getBias ()

    @command(dtype_out=str)
    def getModel (self):
        return self.device_model

    # ATTRIBUTES

    @attribute(dtype=((float,),), max_dim_x=4, max_dim_y=10000000)
    def Buffer(self):
        buf = self.elettra_device.getBuffer()
        if not buf:
            buf = [-1]*4
        return np.array(buf, dtype=np.double)

    @attribute(dtype=(float,), max_dim_x=10000000)
    def TimeStamp(self):
        buf = self.elettra_device.getTimeStamp()
        if not buf:
            buf = [-1]
        return np.array(buf, dtype=np.double)

    @attribute(dtype=(float,), max_dim_x=20)
    def Measure(self):
        measbuf = self.elettra_device.getMeasure(self.ctime)
        if not measbuf:
            measbuf = [-1]
        return np.array(measbuf, dtype=np.double)

    @attribute(dtype=float, unit='A', format='%g')
    def Current1(self):
        measbuf = self.elettra_device.getMeasure(self.ctime)

        if not measbuf:
            return 0
        return measbuf[0]

    @attribute(dtype=float, unit='A', format='%g')
    def Current2(self):
        measbuf = self.elettra_device.getMeasure(self.ctime)

        if not measbuf:
            return 0
        return measbuf[1]

    @attribute(dtype=float, unit='A', format='%g')
    def Current3(self):
        measbuf = self.elettra_device.getMeasure(self.ctime)

        if not measbuf:
            return 0
        return measbuf[2]

    @attribute(dtype=float, unit='A', format='%g')
    def Current4(self):
        measbuf = self.elettra_device.getMeasure(self.ctime)

        if not measbuf:
            return 0
        return measbuf[3]

    @attribute(dtype=float, unit='A', format='%g')
    def CurrentTot(self):
        measbuf = self.elettra_device.getMeasure(self.ctime)
        if not measbuf:
            return 0

        return math.fsum(measbuf[0:4])

    @attribute(dtype=float, unit='', format='%g')
    def Y(self):
        measbuf = self.elettra_device.getMeasure(self.ctime)
        if not measbuf:
            return 0

        return self.elettra_device.getY(measbuf)

    @attribute(dtype=float, unit='', format='%g')
    def Z(self):
        measbuf = self.elettra_device.getMeasure(self.ctime)
        if not measbuf:
            return 0

        return self.elettra_device.getZ(measbuf)

def main():
    import sys
    py = tango.Util(sys.argv)
    def do_gevent():
        gevent.wait(count=1,timeout=0.003)

    py.server_set_event_loop(do_gevent)

    tango.server.run((ElettraElectrometer,))

