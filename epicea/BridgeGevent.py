###
#      BridgeGevent.py
#
#      This modules allows to proxy - bridge a python class object used by a Tango device server.
#      When the object is created and access through the bridge gevent greenlets started by the 
#      object do not interfere with Tango execution
#
#      Usage:
#         In your Tango server module add:
#          
#         at the top of your server module: 
#
#             from BridgeGevent import BridgeGevent
#
#             bridge = BridgeGevent()
#             bridge.run()
#
#         create the object with the same parameters through the bridge. 
#             For example;
#                   myobj = MyClassWithGevent(par1, par2, par3="something")
#             becomes:
#                   bridge.create_device(MyClassWithGevent, par1, par2, par3="something")
#                   myobj = bridge
#
#         allow general gevent concurrency on server startup:
#        
#             import tango
#
#             def main():
#                 import sys
#                 py = tango.Util(sys.argv)
#
#                 def do_gevent():
#                     gevent.wait(count=1,timeout=0.003)
#
#                 py.server_set_event_loop(do_gevent)
#   
#                 tango.server.run((ElettraElectrometer,))
#

###
import time

import gevent
from gevent.queue import Queue
from gevent.monkey import patch_all

patch_all()

in_queue = Queue()
out_queue = Queue()

class BridgeCommand(object):

    def __init__(self, _cmd):
        self._cmd = _cmd

    def __call__(self, *args, **kwargs):
        in_queue.put(("run", [self._cmd, args, kwargs]))
        while out_queue.empty():
           gevent.sleep(0.005)
        ret = out_queue.get()
        return ret

class BridgeGevent(object):

    MAX_TIME_CREATE = 2

    def __init__(self):
        self.device = None
        self.t = None
        self.device_created = False

    def run(self):
        self.is_running = True
        gevent.spawn(self.update)

    def stop(self):
        self.is_running = False

    def create_device(self, cls, *args, **kwargs):
        self.device_created = False
        in_queue.put(('new', [cls, args, kwargs]))
 
        # wait device creation finishes
        t0 = time.time()
        while time.time() - t0 < self.MAX_TIME_CREATE:
            if self.device_created:
                 break
            gevent.sleep(0.005)

    def __getattr__(self, attr):
        if self.device is None:
            print("cannot execute commmand. create a device first")
        else:
            _c = getattr(self.device, attr) 
            return BridgeCommand(_c)

    def update(self):
        t = None
        while self.is_running:
            if in_queue.empty():
                gevent.sleep(0.005)
                continue

            cmd, cmd_args = in_queue.get()

            if cmd == 'new':
                cls, args, kwargs = cmd_args
                self.device = cls(*args,**kwargs)
                self.device_created = True
            else:
                cmd, args, kwargs = cmd_args
                t = gevent.spawn(cmd, *args, **kwargs)
                t.link(self.finished)
                t.link_exception(self.failure)

    def finished(self,t):
        res = t.get()
        out_queue.put(res)

    def failure(self,t):
        try:
           res = t.get()
        except BaseException as e:
           res = "error:" + str(e)
        out_queue.put(res)
 
class TestClass(object):
    def __init__(self):
        pass

    def cmd1(self):
        print("cmd1 witch bck task spawned")
        gevent.spawn(self.backg_task)
        gevent.sleep(3)
        print("cmd1 done")
        return 1

    def cmd2(self):
        print("cmd2")
        gevent.sleep(2)
        print("cmd2 done")
        return 2

    def cmd3(self):
        gevent.sleep(1)
        y = 5/0
        print("cmd3 done")
        return 3

    def backg_task(self):
        t0 = time.time()
        while True:
           print("bck") 
           if (time.time() - t0) > 7: 
                break
           gevent.sleep(1)

def test():

    b = BridgeGevent()
    b.run()
    b.create_device(TestClass)
    gevent.sleep(0.1)

    got_t1 = got_t2 = False

    print("cmd1", b.cmd1())
    print("cmd2", b.cmd2())
    print("cmd3", b.cmd3())

if __name__ == '__main__':
    test()
